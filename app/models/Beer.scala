package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class Beer(name: String)

object Beer {
  implicit val format: Format[Beer] = Json.format[Beer]
}