package plugins.couchbase

import play.api.mvc.Controller

import play.api.Play.current

trait CouchbaseController {
  self: Controller =>

  def db_client = CouchbasePlugin.db_client

}
