package plugins.couchbase

import play.api._

import com.couchbase.client.CouchbaseClient
import scala.concurrent.ExecutionContext
import scala.collection.mutable.ArrayBuffer

import java.net.URI

import collection.JavaConversions._
import collection.mutable.ArrayBuffer

class CouchbasePlugin(app: Application) extends Plugin {

  private var _helper : Option[CouchbaseConnectionHelper] = None

  def helper = _helper.getOrElse(throw new RuntimeException("Plugin not available yet."))

  override def onStart {
    Logger info "Couchbase plugin starting."
    _helper = {
      val conf = CouchbasePlugin.parseConfig(app)
      try {
        Some(CouchbaseConnectionHelper(conf._1, conf._2, conf._3, conf._4))
      } catch {
        case e: Throwable => {
          throw new PlayException("CouchbasePlugin initialization error", "Failed to connect to database.", e)
        }
      }
    }

  }

  override def onStop {
    import java.util.concurrent.TimeUnit

    Logger info "Couchbase plugin stopping."
    _helper.map { h =>
      h.db_client.shutdown(1, TimeUnit.SECONDS)
    }

    _helper = None
  }

}

object CouchbasePlugin {
  def db_client(implicit app: Application) = curr_instance.helper.db_client

  def curr_instance(implicit app: Application): CouchbasePlugin = app.plugin[CouchbasePlugin] match {
    case Some(plugin) => plugin
    case _            => throw new PlayException("CouchbasePlugin error", "Error with CouchbasePlugin. Failed to initialize.")
  }

  def curr_instance(app: play.Application): CouchbasePlugin = app.plugin(classOf[CouchbasePlugin]) match {
    case plugin if plugin != null => plugin
    case _                        => throw new PlayException("CouchbasePlugin Error", "The CouchbasePlugin has not been initialized! Please edit your conf/play.plugins file and add the following line: '400:plugins.couchbase.CouchbasePlugin' (400 is an arbitrary priority and may be changed to match your needs).")
  }

  private def parseConfig(app: Application): (List[String], Int, String, String) = {
      (app.configuration.getStringList("couchbase.servers") match {
          case Some(list) => scala.collection.JavaConversions.collectionAsScalaIterable(list).toList
          case None => List("localhost")
        },
        app.configuration.getInt("couchbase.port").getOrElse(8091),
        app.configuration.getString("couchbase.bucket").getOrElse("default"),
        app.configuration.getString("couchbase.password").getOrElse(""))
  }
}


private[couchbase] case class CouchbaseConnectionHelper(servers: List[String], port: Int, bucket: String = "default", password: String = "") {
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global
  var uris = List[URI]()
  servers map { server =>
    Logger info "http://" + server + ":" + port + "/pools"
    uris = URI.create("http://" + server + ":" + port + "/pools") :: uris
  }

  lazy val db_client = new CouchbaseClient(uris, bucket, password)
}