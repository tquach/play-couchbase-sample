package controllers


import play.api.mvc._
import play.api.libs.json._
import play.api._
import play.api.mvc._
import play.api.libs.json._

import play.api.libs.functional.syntax._

import plugins.couchbase.CouchbaseController
import org.codehaus.jackson.map.ObjectMapper
import com.couchbase.client.protocol.views.{ViewResponse, Query}
import models.Beer
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import net.spy.memcached.internal.OperationFuture

object BreweryController extends Controller with CouchbaseController {
  val mapper: ObjectMapper = new ObjectMapper()

  def index = Action {
    Ok(Json.obj("status" -> "OK"))
  }

  def find(id: String) = Action {
    // val lists = db_client.get("lists")
    Logger info "Finding list with id " + id

    val doc = Some(db_client.get(id))
    Logger info "Retrieved JS document %s".format(doc)

    val doc_js: JsValue = Json.parse(doc.toString)
    Ok(doc_js)
  }

  def save() = Action(parse.tolerantJson) { request =>
    Logger info "Request body " + request.body
    request.body.validate[Beer].fold (
      valid = { res =>
        var setFuture: OperationFuture[java.lang.Boolean] = db_client.set(res.name, 0, Json.toJson(res))
        Ok(res.name)
      },
      invalid = ( e => BadRequest(e.toString) )
    )
  }
}